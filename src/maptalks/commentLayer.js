import {VectorLayer, Marker} from 'maptalks'
import {slice} from '../utils/string'

const pointSymbol = {
  'markerType': 'ellipse',
  'markerFill': '#ffff00',
  'markerFillOpacity': 1,
  'markerLineColor': '#ffff00',
  'markerLineWidth': 0,
  'markerLineOpacity': 1,
  'markerWidth': 8,
  'markerHeight': 8,
  'markerDx': 0,
  'markerDy': 0,
  'markerOpacity': 1,
  'shadowBlur': 3,
  'shadowOffsetX': 0,
  'shadowOffsety': 0
}

const iconSymbol = {
  'markerType': 'path',
  'markerPath': 'M512 144.794c-247.162 0-447.527 161.007-447.527 359.62 0 82.45 34.532 158.417 92.605 219.078l-0.264 1.396-22.58 119.482c-4.195 22.198 17.213 40.612 38.534 33.146l114.764-40.186 27.887-9.765c59.339 23.353 126.052 36.469 196.58 36.469 247.162 0 447.527-161.007 447.527-359.62S759.162 144.794 512 144.794z',
  'markerPathWidth': 1024,
  'markerPathHeight': 1024,
  'markerWidth': 16,
  'markerHeight': 16,
  'markerDx': 0,
  'markerDy': 8,
  'markerOpacity': 1,
  'markerFill': '#ffff00',
  'shadowBlur': 3,
  'shadowOffsetX': 0,
  'shadowOffsety': 0
}

const textSymbol = {
  'textFaceName': 'sans-serif',
  'textName': '{content}',
  'textWeight': 'bold',
  'textSize': 14,
  'textFont': null,
  'textFill': '#ffff00',
  'textOpacity': 1,
  // 'textHaloFill': '#fff',
  // 'textHaloRadius': 5,
  'textWrapWidth': null,
  'textWrapCharacter': '\n',
  'textLineSpacing': 0,
  'shadowBlur': 3,
  'shadowOffsetX': 0,
  'shadowOffsety': 0
}

export class AgeeyeCommentLayer {
  constructor(map){
    this.map = map
    this.commentLayer = new VectorLayer('comment')
    this.map.addLayer(this.commentLayer)
    this.bindEvent()
  }

  bindEvent () {
    this.map.on('viewchange', event => this.onViewChange(event))
  }

  onViewChange (event) {
    if (event.old.zoom === event.new.zoom) {
      return false
    }

    this.updateSymbol()
  }

  getSymbol(zoom){
    const maxZoom = this.map.getMaxZoom()
    const minZoom = this.map.getMinZoom()

    const zoomRange = maxZoom - minZoom

    if (zoomRange === 0){
      return pointSymbol
    }

    if (zoomRange === 1) {
      return zoom === maxZoom ? iconSymbol : pointSymbol
    }

    if (zoom === maxZoom) {
      return textSymbol
    } else if (zoom === minZoom) {
      return pointSymbol
    } else {
      return iconSymbol
    }
  }

  addComment(comment){
    const {id, lat, lng, content} = comment
    const marker = new Marker([lng, lat], {
      id: id,
      properties: {
        content: this.handleCommentContent(content)
      },
      symbol: pointSymbol
    })

    marker.on({
      click: event => this.onCommentClick(event)
    })

    this.commentLayer.addGeometry(marker)
  }

  setComments(comments){
    const commentLayer = this.commentLayer

    commentLayer.clear()

    comments.forEach(comment => {
      this.addComment(comment)
    })
  }

  handleCommentContent(content){
    content = content.replace(/<p>/g, '')
    content = content.replace(/<\/p>/g, '')

    return slice(content, 10)
  }

  updateSymbol(){
    const zoom = this.map.getZoom()
    const symbol = this.getSymbol(zoom)

    const geometries = this.commentLayer.getGeometries()
    geometries.forEach(geometry => {
      geometry.setSymbol(symbol)
    })
  }

  show(){
    this.commentLayer.show()
  }

  hide(){
    this.commentLayer.hide()
  }

  on(params){
    this.commentLayer.on(params)
  }

  onCommentClick(event){
    const id = event.target.getId()
    this.commentLayer.fire('commentClick', {id})
  }
}
