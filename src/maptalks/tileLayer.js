import {getCssFilter, getTileCode, getTileCompany} from './util'
import {TileLayer} from 'maptalks'

export class AgeeyeTileLayer {
  constructor(map, tileLayerData){
    this.map = map

    const baseTileLayerOptions = this._getTileOptions(tileLayerData.base)
    const highTileLayerOptions = this._getTileOptions(tileLayerData.high)

    if (baseTileLayerOptions) {
      this.baseTileLayer = this._getTileLayer('base', baseTileLayerOptions)
      this.map.setBaseLayer(this.baseTileLayer)
    }

    if (highTileLayerOptions) {
      this.highTileLayer = this._getTileLayer('high', highTileLayerOptions)
      this.map.addLayer(this.highTileLayer)
    }
  }

  _initTileLayer (options) {

  }

  _getTileOptions (tileData) {
    const {name, opacity, cssFilter} = tileData

    if (!name) {
      return
    }

    const company = getTileCompany(name)
    // const companyConfig = config[company]
    const options = {opacity}

    if (cssFilter) {
      options.cssFilter = getCssFilter(cssFilter)
    }

    if (company === 'image') {
      const tileCode = getTileCode(name, 'image')
      options.urlTemplate = `https://ageeye-map-image.oss-cn-hangzhou.aliyuncs.com/${tileCode}/{x}_{y}_{z}.jpg`
      options.renderer = 'canvas'
    }

    return options
  }

  _getTileLayer (type, options) {
    return new TileLayer(type, options)
  }
}
