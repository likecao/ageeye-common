import tileLayers from './config/tileLayers'

export function iterCoord(coord, callback) {
  if (coord[0] instanceof Array) {
    coord.forEach(value => {
      iterCoord(value, callback)
    })
  } else {
    callback(coord)
  }
}

export function getTileCompany(arg) {
  if (arg == null){
    return null
  }

  let type

  Object.keys(tileLayers).forEach(value => {
    if (arg.search('^' + value) === 0) {
      type = value
      return false
    }
  })

  return type
}

export function getTileCode (arg, type) {
  return arg.replace(`${type}-`, '')
}

export function getCssFilter (cssFilter) {
  let result = []

  if (cssFilter) {
    Object.keys(cssFilter).forEach(name => {
      if (name === 'hue-rotate') {
        result.push(name + '(' + cssFilter[name] + 'deg)')
      } else if (name === 'blur') {
        result.push(name + '(' + cssFilter[name] + 'px)')
      } else {
        result.push(name + '(' + cssFilter[name] + '%)')
      }
    })

    return result.join(' ')
  } else {
    return ''
  }
}
