/**
 * Created by CL on 2017/7/11.
 */
const getUrl = ({x, y, z, s, url}) => {
  y = Math.pow(2, z) - 1 - y
  return url.format({
    s,
    z,
    x,
    y,
    x16: parseInt(x / 16),
    y16: parseInt(y / 16)
  })
}

export default {
  qq: {
    title: '腾讯地图',
    projection: 'DEFAULT',
    company: 'qq',
    link: 'http://map.qq.com',
    attribution: '<a target="_blank" href="http://map.qq.com">腾讯地图</a>',
    subdomains: ['1', '2', '3'],
    image: true,
    tiles: {
      qqStreet: {
        title: '街道',
        url: (x, y, z, domain) => {
          const url = 'https://rt{s}.map.gtimg.com/realtimerender?z={z}&x={x}&y={y}&type=vector&style=0'
          return url.format({
            x,
            y: Math.pow(2, z) - 1 - y,
            z,
            s: domain
          })
        }
      },
      qqTerrainSimple: {
        title: '地形',
        url: (x, y, z, domain) => {
          return getUrl({
            x,
            y,
            z,
            s: domain,
            url: 'https://p{s}.map.gtimg.com/demTiles/{z}/{x16}/{y16}/{x}_{y}.jpg'
          })
        }
      },
      qqTerrainStreet: {
        title: '地形街道',
        url: (x, y, z, domain) => {
          return getUrl({
            x,
            y,
            z,
            s: domain,
            url: 'https://p{s}.map.gtimg.com/demTiles/{z}/{x16}/{y16}/{x}_{y}.jpg'
          })
        },
        url2: (x, y, z, domain) => {
          return getUrl({
            x,
            y,
            z,
            s: domain,
            url: 'https://rt{s}.map.gtimg.com/tile?z={z}&x={x}&y={y}&type=vector&styleid=3&version=110'
          })
        }
      },
      qqSatellite: {
        title: '卫星',
        url: (x, y, z, domain) => {
          return getUrl({
            x,
            y,
            z,
            s: domain,
            url: 'https://p{s}.map.gtimg.com/sateTiles/{z}/{x16}/{y16}/{x}_{y}.jpg'
          })
        }
      }
    }
  },
  google: {
    title: '谷歌地图',
    projection: 'DEFAULT',
    link: 'http://ditu.google.cn',
    attribution: '<a target="_blank" href="http://ditu.google.cn">谷歌地图</a>',
    image: true,
    tiles: {
      googleTerrainSimple: {
        title: '地形',
        url: 'https://mt{s}.google.cn/vt?pb=!1m4!1m3!1i{z}!2i{x}!3i{y}!2m3!1e4!2st!3i132!2m3!1e0!2sr!3i285205865!3m14!2szh-CN!3sCN!5e18!12m1!1e63!12m3!1e37!2m1!1ssmartmaps!12m4!1e26!2m2!1sstyles!2zcy50OjN8cC52Om9mZixzLnQ6MXxwLnY6b2ZmLHMudDoyfHAudjpvZmY!4e0'
      },
      googleStreet: {
        title: '街道',
        url: 'https://mt{s}.google.cn/vt/lyrs=m@167000000&hl=zh-CN&gl=cn&x={x}&y={y}&z={z}&s=Galil'
      },
      googleTerrainStreet: {
        title: '地形街道',
        url: 'https://mt{s}.google.cn/vt/lyrs=t@128,r@176000000&hl=zh-CN&gl=cn&src=app&x={x}&y={y}&z={z}&s=Galil'
      },
      googleSimple: {
        title: '简单',
        url: 'https://mt{s}.google.cn/vt?pb=!1m4!1m3!1i{z}!2i{x}!3i{y}!2m3!1e0!2sm!3i285000000!3m14!2szh-CN!3sCN!5e18!12m1!1e47!12m3!1e37!2m1!1ssmartmaps!12m4!1e26!2m2!1sstyles!2zcy50OjN8cC52Om9mZixzLnQ6MXxwLnY6b2ZmLHMudDoyfHAudjpvZmY!4e0'
      },
      googleSatellite: {
        title: '卫星',
        url: 'https://www.google.cn/maps/vt?lyrs=s@197&gl=cn&x={x}&y={y}&z={z}'
      },
      /*
       [
       {
       "featureType": "administrative.country",
       "stylers": [
       {
       "weight": 0.5
       }
       ]
       },
       {
       "featureType": "administrative.country",
       "elementType": "labels.text.fill",
       "stylers": [
       {
       "color": "#a2a2a2"
       }
       ]
       },
       {
       "featureType": "administrative.locality",
       "elementType": "labels.text.fill",
       "stylers": [
       {
       "color": "#858585"
       }
       ]
       },
       {
       "featureType": "administrative.province",
       "elementType": "labels.text.fill",
       "stylers": [
       {
       "color": "#a2a2a2"
       }
       ]
       },
       {
       "featureType": "landscape.natural.terrain",
       "stylers": [
       {
       "saturation": -100
       }
       ]
       },
       {
       "featureType": "road.highway",
       "stylers": [
       {
       "saturation": -100
       },
       {
       "lightness": 70
       },
       {
       "weight": 0.5
       }
       ]
       },
       {
       "featureType": "transit.line",
       "stylers": [
       {
       "color": "#858585"
       }
       ]
       }
       ]
       */
      googleLight: {
        title: '浅灰',
        url: 'https://mt{s}.google.cn/maps/vt?pb=!1m5!1m4!1i{z}!2i{x}!3i{y}!4i256!2m3!1e0!2sm!3i411111422!3m14!2szh-CN!3sCN!5e18!12m1!1e68!12m3!1e37!2m1!1ssmartmaps!12m4!1e26!2m2!1sstyles!2zcy50OjE3fHAudzowLjUscy50OjE3fHMuZTpsLnQuZnxwLmM6I2ZmYTJhMmEyLHMudDoxOXxzLmU6bC50LmZ8cC5jOiNmZjg1ODU4NSxzLnQ6MTl8cy5lOmwudC5zfHAuYzojZmZmZmZmZmZ8cC53OjMuNSxzLnQ6MTh8cy5lOmcuc3xwLmM6I2ZmYjRiNGI0LHMudDoxOHxzLmU6bC50LmZ8cC5jOiNmZmEyYTJhMixzLnQ6MTMxNHxwLnM6LTEwMCxzLnQ6NDl8cC5zOi0xMDB8cC5sOjcwfHAudzowLjUscy50OjY1fHAuYzojZmY4NTg1ODU!4e0'
      },
      /*
       [
       {
       "elementType": "geometry",
       "stylers": [
       {
       "color": "#ebe3cd"
       }
       ]
       },
       {
       "featureType": "administrative",
       "elementType": "geometry.stroke",
       "stylers": [
       {
       "color": "#c9b2a6"
       }
       ]
       },
       {
       "featureType": "administrative",
       "elementType": "labels.icon",
       "stylers": [
       {
       "color": "#d6c69a"
       }
       ]
       },
       {
       "featureType": "administrative",
       "elementType": "labels.text.fill",
       "stylers": [
       {
       "color": "#ad9974"
       }
       ]
       },
       {
       "featureType": "landscape.natural",
       "elementType": "geometry",
       "stylers": [
       {
       "color": "#dfd1ac"
       }
       ]
       },
       {
       "featureType": "landscape.natural.terrain",
       "elementType": "geometry",
       "stylers": [
       {
       "color": "#ebe3c5"
       }
       ]
       },
       {
       "featureType": "poi",
       "elementType": "geometry",
       "stylers": [
       {
       "color": "#dfd2ae"
       }
       ]
       },
       {
       "featureType": "poi",
       "elementType": "labels.text.fill",
       "stylers": [
       {
       "color": "#d2bf7d"
       }
       ]
       },
       {
       "featureType": "poi.park",
       "elementType": "geometry.fill",
       "stylers": [
       {
       "color": "#c9ce8a"
       }
       ]
       },
       {
       "featureType": "poi.park",
       "elementType": "labels.text.fill",
       "stylers": [
       {
       "color": "#c6ce79"
       }
       ]
       },
       {
       "featureType": "road.highway",
       "stylers": [
       {
       "color": "#e3d7b7"
       }
       ]
       },
       {
       "featureType": "transit.line",
       "elementType": "geometry",
       "stylers": [
       {
       "color": "#dfd2ae"
       }
       ]
       },
       {
       "featureType": "transit.line",
       "elementType": "labels.text.fill",
       "stylers": [
       {
       "color": "#8f7d77"
       }
       ]
       },
       {
       "featureType": "transit.line",
       "elementType": "labels.text.stroke",
       "stylers": [
       {
       "color": "#ebe3cd"
       }
       ]
       },
       {
       "featureType": "transit.station",
       "elementType": "geometry",
       "stylers": [
       {
       "color": "#dfd2ae"
       }
       ]
       },
       {
       "featureType": "water",
       "elementType": "geometry.fill",
       "stylers": [
       {
       "color": "#d5e9cd"
       }
       ]
       },
       {
       "featureType": "water",
       "elementType": "labels.icon",
       "stylers": [
       {
       "color": "#b9a675"
       }
       ]
       },
       {
       "featureType": "water",
       "elementType": "labels.text.fill",
       "stylers": [
       {
       "color": "#dabf70"
       }
       ]
       }
       ]
       */
      googleRetro: {
        title: '复古',
        url: 'https://mt{s}.google.cn/maps/vt?pb=!1m5!1m4!1i{z}!2i{x}!3i{y}!4i256!2m3!1e0!2sm!3i411111338!3m14!2szh-CN!3sCN!5e18!12m1!1e68!12m3!1e37!2m1!1ssmartmaps!12m4!1e26!2m2!1sstyles!2zcy5lOmd8cC5jOiNmZmViZTNjZCxzLnQ6MXxzLmU6Zy5zfHAuYzojZmZjOWIyYTYscy50OjF8cy5lOmwuaXxwLmM6I2ZmZDZjNjlhLHMudDoxfHMuZTpsLnQuZnxwLmM6I2ZmYWQ5OTc0LHMudDo4MnxzLmU6Z3xwLmM6I2ZmZGZkMWFjLHMudDoxMzE0fHMuZTpnfHAuYzojZmZlYmUzYzUscy50OjJ8cy5lOmd8cC5jOiNmZmRmZDJhZSxzLnQ6MnxzLmU6bC50LmZ8cC5jOiNmZmQyYmY3ZCxzLnQ6NDB8cy5lOmcuZnxwLmM6I2ZmYzljZThhLHMudDo0MHxzLmU6bC50LmZ8cC5jOiNmZmM2Y2U3OSxzLnQ6NDl8cC5jOiNmZmUzZDdiNyxzLnQ6NjV8cy5lOmd8cC5jOiNmZmRmZDJhZSxzLnQ6NjV8cy5lOmwudC5mfHAuYzojZmY4ZjdkNzcscy50OjY1fHMuZTpsLnQuc3xwLmM6I2ZmZWJlM2NkLHMudDo2NnxzLmU6Z3xwLmM6I2ZmZGZkMmFlLHMudDo2fHMuZTpnLmZ8cC5jOiNmZmQ1ZTljZCxzLnQ6NnxzLmU6bC5pfHAuYzojZmZiOWE2NzUscy50OjZ8cy5lOmwudC5mfHAuYzojZmZkYWJmNzA!4e0'
      },
      /*
       [
       {
       "elementType": "geometry",
       "stylers": [
       {
       "color": "#212121"
       }
       ]
       },
       {
       "elementType": "labels.icon",
       "stylers": [
       {
       "visibility": "off"
       }
       ]
       },
       {
       "elementType": "labels.text.fill",
       "stylers": [
       {
       "color": "#757575"
       }
       ]
       },
       {
       "elementType": "labels.text.stroke",
       "stylers": [
       {
       "color": "#212121"
       }
       ]
       },
       {
       "featureType": "administrative",
       "elementType": "labels.text.fill",
       "stylers": [
       {
       "color": "#5e5e5e"
       }
       ]
       },
       {
       "featureType": "administrative.country",
       "elementType": "geometry.stroke",
       "stylers": [
       {
       "color": "#3c3c3c"
       }
       ]
       },
       {
       "featureType": "administrative.land_parcel",
       "stylers": [
       {
       "visibility": "off"
       }
       ]
       },
       {
       "featureType": "landscape.natural.terrain",
       "elementType": "geometry.fill",
       "stylers": [
       {
       "color": "#000000"
       }
       ]
       },
       {
       "featureType": "poi",
       "elementType": "labels.text.fill",
       "stylers": [
       {
       "color": "#757575"
       }
       ]
       },
       {
       "featureType": "poi.park",
       "elementType": "geometry",
       "stylers": [
       {
       "color": "#181818"
       }
       ]
       },
       {
       "featureType": "poi.park",
       "elementType": "labels.text.fill",
       "stylers": [
       {
       "color": "#616161"
       }
       ]
       },
       {
       "featureType": "poi.park",
       "elementType": "labels.text.stroke",
       "stylers": [
       {
       "color": "#1b1b1b"
       }
       ]
       },
       {
       "featureType": "road",
       "stylers": [
       {
       "lightness": -100
       }
       ]
       },
       {
       "featureType": "road",
       "elementType": "geometry.fill",
       "stylers": [
       {
       "color": "#2c2c2c"
       }
       ]
       },
       {
       "featureType": "road",
       "elementType": "labels.text.fill",
       "stylers": [
       {
       "color": "#8a8a8a"
       }
       ]
       },
       {
       "featureType": "road.arterial",
       "elementType": "geometry",
       "stylers": [
       {
       "color": "#373737"
       }
       ]
       },
       {
       "featureType": "road.arterial",
       "elementType": "geometry.fill",
       "stylers": [
       {
       "lightness": -100
       }
       ]
       },
       {
       "featureType": "road.highway",
       "elementType": "geometry",
       "stylers": [
       {
       "color": "#3c3c3c"
       }
       ]
       },
       {
       "featureType": "road.highway",
       "elementType": "geometry.fill",
       "stylers": [
       {
       "lightness": -35
       }
       ]
       },
       {
       "featureType": "road.highway",
       "elementType": "geometry.stroke",
       "stylers": [
       {
       "lightness": -35
       }
       ]
       },
       {
       "featureType": "transit",
       "elementType": "geometry.fill",
       "stylers": [
       {
       "lightness": -100
       }
       ]
       },
       {
       "featureType": "transit",
       "elementType": "labels.text.fill",
       "stylers": [
       {
       "color": "#757575"
       }
       ]
       },
       {
       "featureType": "transit.line",
       "elementType": "geometry.fill",
       "stylers": [
       {
       "lightness": -100
       }
       ]
       },
       {
       "featureType": "transit.line",
       "elementType": "geometry.stroke",
       "stylers": [
       {
       "color": "#9e9e9e"
       }
       ]
       },
       {
       "featureType": "transit.station",
       "elementType": "geometry.fill",
       "stylers": [
       {
       "lightness": -100
       }
       ]
       },
       {
       "featureType": "water",
       "elementType": "geometry",
       "stylers": [
       {
       "color": "#000000"
       }
       ]
       },
       {
       "featureType": "water",
       "elementType": "labels.text.fill",
       "stylers": [
       {
       "color": "#3d3d3d"
       }
       ]
       }
       ]
       */
      googleDark: {
        title: '深灰',
        url: 'https://mt{s}.google.cn/maps/vt?pb=!1m5!1m4!1i{z}!2i{x}!3i{y}!4i256!2m3!1e0!2sm!3i411111338!3m14!2szh-CN!3sCN!5e18!12m1!1e68!12m3!1e37!2m1!1ssmartmaps!12m4!1e26!2m2!1sstyles!2zcy5lOmd8cC5jOiNmZjIxMjEyMSxzLmU6bC5pfHAudjpvZmYscy5lOmwudC5mfHAuYzojZmY3NTc1NzUscy5lOmwudC5zfHAuYzojZmYyMTIxMjEscy50OjF8cy5lOmwudC5mfHAuYzojZmY1ZTVlNWUscy50OjE3fHMuZTpnLnN8cC5jOiNmZjNjM2MzYyxzLnQ6MjF8cC52Om9mZixzLnQ6MTMxNHxzLmU6Zy5mfHAuYzojZmYwMDAwMDAscy50OjJ8cy5lOmwudC5mfHAuYzojZmY3NTc1NzUscy50OjQwfHMuZTpnfHAuYzojZmYxODE4MTgscy50OjQwfHMuZTpsLnQuZnxwLmM6I2ZmNjE2MTYxLHMudDo0MHxzLmU6bC50LnN8cC5jOiNmZjFiMWIxYixzLnQ6M3xwLmw6LTEwMCxzLnQ6M3xzLmU6Zy5mfHAuYzojZmYyYzJjMmMscy50OjN8cy5lOmwudC5mfHAuYzojZmY4YThhOGEscy50OjUwfHMuZTpnfHAuYzojZmYzNzM3Mzcscy50OjUwfHMuZTpnLmZ8cC5sOi0xMDAscy50OjQ5fHMuZTpnfHAuYzojZmYzYzNjM2Mscy50OjQ5fHMuZTpnLmZ8cC5sOi0zNSxzLnQ6NDl8cy5lOmcuc3xwLmw6LTM1LHMudDo0fHMuZTpnLmZ8cC5sOi0xMDAscy50OjR8cy5lOmwudC5mfHAuYzojZmY3NTc1NzUscy50OjY1fHMuZTpnLmZ8cC5sOi0xMDAscy50OjY1fHMuZTpnLnN8cC5jOiNmZjllOWU5ZSxzLnQ6NjZ8cy5lOmcuZnxwLmw6LTEwMCxzLnQ6NnxzLmU6Z3xwLmM6I2ZmMDAwMDAwLHMudDo2fHMuZTpsLnQuZnxwLmM6I2ZmM2QzZDNk!4e0'
      }
    }
  },
  tianDiTu: {
    title: '天地图',
    projection: 'DEFAULT',
    link: 'http://www.tianditu.com/',
    attribution: '<a target="_blank" href="http://www.tianditu.com/">天地图</a>',
    image: true,
    tiles: {
      tianDiTuTerrain: {
        title: '地形',
        url: 'http://t{s}.tianditu.com/DataServer?T=ter_w&X={x}&Y={y}&L={z}'
      },
      tianDiTuTerrainStreet: {
        title: '地形街道',
        url: 'http://t{s}.tianditu.com/DataServer?T=ter_w&X={x}&Y={y}&L={z}',
        url2: 'http://t{s}.tianditu.com/DataServer?T=cta_w&X={x}&Y={y}&L={z}'
      }
    }
  },
  // webdog: {
  //   title: "发现中国",
  //   tiles: {
  //     webdogColorLightGrey: {
  //       title: '浅灰色',
  //       url: '/static/image/white-256x256.jpg'
  //     },
  //     webdogColorDarkGrey: {
  //       title: '深灰色',
  //       url: '/static/image/black-256x256.jpg'
  //     },
  //     webdogColorBlue: {
  //       title: '蓝色',
  //       url: '/static/image/blue-256x256.jpg'
  //     },
  //     webdogColorBrown: {
  //       title: '棕色',
  //       url: '/static/image/brown-256x256.jpg'
  //     },
  //   }
  // },
  maplet: {
    title: '地图云集',
    projection: 'DEFAULT',
    link: 'http://www.maplet.org/',
    wms: 'http://wcsa.osgeo.cn:8088/service?',
    format: 'image/png',
    transparent: true,
    renderer: 'canvas',
    attribution: 'Map &copy; <a href="http://www.osgeo.cn/">OSGeo China</a>',
    image: false,
    tiles: 'ajax'
  },
  image: {
    title: '图片',
    projection: 'IDENTITY',
    link: 'https://www.ageeye.cn/',
    format: 'image/jpeg',
    image: false,
    attribution: 'Map &copy; <a href="https://www.ageeye.cn/">Age Eye</a>',
    tiles: 'ajax'
  },
  ccts: {
    title: '中央研究院',
    projection: 'DEFAULT',
    link: 'http://gis.sinica.edu.tw',
    maxZoom: 12,
    renderer: 'canvas',
    urlTemplate: 'http://gis.sinica.edu.tw/ccts/file-exists.php?img={arg}-png-{z}-{x}-{y}',
    image: false,
    tiles: {
      cctsXiHan: {
        title: '西汉 7年',
        url: 'bc0007',
        begin: 202,
        end: 210
      },
      cctsDongHan: {
        title: '东汉 140年',
        url: 'ad0140',
        begin: 25,
        end: 220
      },
      cctsSanGuo: {
        title: '三国 262年',
        url: 'ad0262',
        begin: 220,
        end: 280
      },
      cctsXiJin: {
        title: '西晋 281年',
        url: 'ad0281',
        begin: 265,
        end: 316
      },
      cctsDongJin: {
        title: '东晋 382年',
        url: 'ad0382',
        begin: 317,
        end: 420
      },
      cctsNanBeiChao: {
        title: '南北朝 497年',
        url: 'ad0497',
        begin: 420,
        end: 589
      },
      cctsSui: {
        title: '隋代 612年',
        url: 'ad0612',
        begin: 581,
        end: 619
      },
      cctsTan: {
        title: '唐代 741年',
        url: 'ad0741',
        begin: 618,
        end: 907
      },
      cctsTangJiaoTong: {
        title: '唐代交通路线',
        url: 'http://gis.sinica.edu.tw/googlemap/tang_jpg_7-12/{z}/{x}/IMG_{x}_{y}_{z}.jpg',
        minZoom: 5,
        maxZoom: 10,
        getUrlArgs: function (tilePoint) {
          return {
            z: 17 - tilePoint.z,
            x: tilePoint.x,
            y: tilePoint.y
          }
        }
      },
      cctsBeiSong: {
        title: '北宋 1111年',
        url: 'ad1111',
        begin: 960,
        end: 1127
      },
      cctsNanSong: {
        title: '南宋 1208年',
        url: 'ad1208',
        begin: 1127,
        end: 1279
      },
      cctsYuan: {
        title: '元代 1330年',
        url: 'ad1330',
        begin: 1271,
        end: 1368
      },
      cctsMing: {
        title: '明代 1582年',
        url: 'ad1582',
        begin: 1368,
        end: 1644
      },
      cctsQing: {
        title: '清代 1820年',
        url: 'ad1820',
        begin: 1636,
        end: 1912
      },
      cctsQingMo: {
        title: '清末 1903年',
        url: 'China_Map_1903'
      },
      cctsMinChu: {
        title: '民初',
        url: 'spaper'
      },
      cctsMinGuo1934: {
        title: '民国 1934年',
        url: 'ShunPao_Human_Geo'
      },
      cctsMinGuoAdmin: {
        title: '民国政治区划',
        url: 'tile_ChinaAdmin'
      }
    }
  }
}

