
import tileLayers from './tileLayers'
import times from './times'

const config = {
  times,
  tileLayers
}

// Object.assign(config, editor)

export default config
