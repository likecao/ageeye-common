/**
 * Created by CL on 2018/5/26.
 */
import {Map, Extent, Coordinate} from 'maptalks'
import {iterCoord} from './util'
import {AgeeyeTileLayer} from './tileLayer'
import {AgeeyeCommentLayer} from './commentLayer'
// import config from './config'

const defaultOptions = {
}

const defaultMapOptions = {
  zoom: 5,
  center: [108.9, 34.4],
  maxExtent: null,
  touchPitch: false,
  touchRotate: false,
  panAnimation: false,
  zoomControl: false,
  doubleClickZoom: false,
  attribution: false
}

export class AgeeyeMapViewer {
  constructor(container, data, options){
    this._options = this._initOptions(options)

    data = this._initData(data)

    const mapOptions = this.getMapOptions(data)

    this.map = this._initMap(container, mapOptions)
    this.tileLayer = new AgeeyeTileLayer(this.map, data.tileLayer)
    this.commentLayer = new AgeeyeCommentLayer(this.map)
  }

  _initOptions(options){
    return Object.assign({}, defaultOptions, options)
  }

  _initData(data){
    const dataType = typeof data

    if (dataType === 'object') {
      return data
    } else if (dataType === 'string') {
      return JSON.parse(data)
    } else {
      throw new Error('地图数据错误，无法解析')
    }
  }

  _initMap (container, options) {
    const map = new Map(container, options)
    map.on({
      click: (event) => this._onMapClick(event)
    })

    if (process.env.NODE_ENV === 'development') {
      window.map = map
    }

    // new control.Zoom({
    //   position: {'top': 8, 'right': 8},
    //   slider: false,
    //   zoomLevel: false
    // }).addTo(map)

    return map
  }

  getMapOptions(data){
    const {minZoom, maxZoom, maxExtent, spatial} = data

    const {center, zoom} = this._getView(data)
    const options = {
      minZoom, maxZoom, zoom, center
    }

    if (spatial) {
      options.spatialReference = spatial
    }

    if (maxExtent) {
      options.maxExtent = maxExtent
    }

    return Object.assign({}, defaultMapOptions, options, this._options)
  }

  _getView (data){
    let {center, zoom, layers} = data


    if (center !== 'auto' && zoom !== 'auto') {
      if (data.spatial && data.spatial.projection === 'IDENTITY') {
        center = [0, 0]
      } else {
        center = defaultMapOptions.center
      }

      zoom = defaultMapOptions.zoom
    } else if (data.begin) {
      const extent = this._getLayersExtent(layers)

      if (data.center === 'auto') {
        center = extent.getCenter()
      }

      if (data.zoom !== 'auto') {
        zoom = this.map.getFitZoom(extent)
      }
    } else if (data.center === 'auto') {
      center = defaultMapOptions.center
    } else if (data.zoom === 'auto') {
      zoom = defaultMapOptions.zoom
    }

    return {center, zoom}
  }

  _getLayersExtent(layers) {
    let extent = new Extent()

    for (const id in layers) {
      const layer = layers[id]


      if (!layer.coord) {
        return
      }

      iterCoord(layer.coord, (coord) => {
        extent = extent.combine(new Coordinate(coord))
      })
    }

    return extent
  }

  _onMapClick (event) {
    const maxExtent = this.map.getMaxExtent()

    if (maxExtent.contains(event.coordinate)){
      this.map.fire('extentclick', event)
    }
  }
}
