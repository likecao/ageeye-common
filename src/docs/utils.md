# utils

一些公共工具

## 工具列表

| 组件 | 描述 |
| ---- | ---- |
| [request](/utils/request/) | AJAX相关 |
| string | 文本处理相关 |
| time | 时间相关 |
| element | dom相关 |
| auth | 认证、token、cookie相关 |
| object | object相关 |
| random | 随机数生成 |


