# request

使用axios实现的ajax工具，他们大多统一接受一个Object作为参数，并进行解构。


## 列表

| 名称 | 说明 |
| ---- | ---- |
| getQuerySet | 查询数据集合 |
| getRequest | get请求 |
| postRequest | post请求 |
| deteteRequest | delete请求 |
| patchRequest | patch请求 |
| putRequest | put请求 |

## api地址模版

参数属性中大多都包含一个api属性，此属性为请求后端rest api的模版字符串，
当模版字符串中存在{id}或者{type}时，会被替换。

这些api模版字符串都是合法的：

 - /api/article/
 - /api/article/2/
 - /api/article/{id}/
 - /api/{type}/{id}/

## 修改Vue组件data

request工具中的函数大量应用于requestMixin中，因此在请求成功结束后，
存在一些通过修改this中的属性，直接修改Vue组件data值的操作，前提是this存在这些属性存在。

比如getQuerySet会将请求结果中的数据集合数据，写入this.querySet，将数据集总条数，写入
this.count

参数属性中的loading如果为true，则在ajax请求过程中，会将this.loading设置为true，
请求结束后设置为false

## 返回值

所有的函数都是async异步的，默认返回一个Promise对象。如果使用await，则返回一个Object，他包含以下：

| 属性 | 类型 | 默认值 | 说明 |
| ---- | ---- | ---- | ---- |
| status | Number | | HTTP请求返回的状态码 |
| data | Object | | 返回的数据 |

## getQuerySet

请求一个数据查询集合

接受一个object作为参数，请求成功后判断当前vue对象data中是否存在querySet属性，
并使用请求结果进行覆盖，支持的参数属性如下：

| 属性 | 类型 | 默认值 | 说明 |
| ---- | ---- | ---- | ---- |
| api* | String | | API请求地址模版 |
| id | String Number | | 替换API模版字符串中的id |
| type | String | | 替换API模版字符串中的type |
| loading | Boolean | true | 是都修改this的loading值 |
| page | Number | 1 | 页码 |
| limit | Number | 10 | 分页条数 |
| querySet | String | 'querySet' | 使用请求结果修改this中的同名属性名称,如果为空则不修改,用来保存查询集数组对象 |
| count | String | 'querySet' | 使用请求结果修改this中的同名属性名称,如果为空则不修改，用来保存数据集总条数 |
| params | Object | {} | get参数 |
| callback | Function | | 回调函数，会传入返回的data作为参数 |
| mode | String | 'set' | 支持set和add两种模式，set会覆盖this.querySet, add则会使用Array.push把请求结果添加到querySet之后 |

## getRequest

GET请求

接受一个object作为参数，请求成功后判断当前vue对象data中是否存在instance属性，
并使用请求结果进行覆盖，支持的参数属性如下：

| 属性 | 类型 | 默认值 | 说明 |
| ---- | ---- | ---- | ---- |
| api* | String | | API请求地址模版 |
| id | String Number | | 替换API模版字符串中的id |
| type | String | | 替换API模版字符串中的type |
| loading | Boolean | true | 是都修改this的loading值 |
| instance | String | 'instance' | 使用请求结果修改this的中的同名属性名称,如果为空则不修改 |
| params | Object | {} | get参数 |

## postRequest

POST请求

接受一个object作为参数，支持的参数属性如下：

| 属性 | 类型 | 默认值 | 说明 |
| ---- | ---- | ---- | ---- |
| api* | String | | API请求地址模版 |
| id | String Number | | 替换API模版字符串中的id |
| type | String | | 替换API模版字符串中的type |
| loading | Boolean | true | 是都修改this的loading值 |
| data | Object | {} | 请求发送的数据 |
| method | String | 'post' | 默认使用post方法 |


## deleteRequest

DELETE请求

接受一个object作为参数，支持的参数属性如下：

| 属性 | 类型 | 默认值 | 说明 |
| ---- | ---- | ---- | ---- |
| api* | String | | API请求地址模版 |
| id | String Number | | 替换API模版字符串中的id |
| type | String | | 替换API模版字符串中的type |
| loading | Boolean | true | 是都修改this的loading值 |

## patchRequest

PATCH请求

接受一个object作为参数，支持的参数属性如下：

| 属性 | 类型 | 默认值 | 说明 |
| ---- | ---- | ---- | ---- |
| api* | String | | API请求地址模版 |
| id | String Number | | 替换API模版字符串中的id |
| type | String | | 替换API模版字符串中的type |
| data | Object | {} | 请求发送的数据 |
| loading | Boolean | true | 是都修改this的loading值 |

## 示例

```
import {request} from 'ageeye-quarar-common'

const {postRequest} = request

async function action(id, data){
  const {status, data} = await postRequest({
    api: '/api/map/{id}/',
    id
  })

  if (status === 200) {
    console.log('success', data)
  } else {
    console.log('error', data)
  }
}

action(5)

```
