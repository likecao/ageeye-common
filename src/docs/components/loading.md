# Loading

显示正在加载的动画效果

## 组件

| 组件 | 描述 |
| ---- | ---- |
| ALoading |  显示正在加载的动画效果 |

## ALoading

显示正在加载的动画效果

### 属性

| 属性 | 类型 | 默认值 | 描述 |
| ---- | ---- | ---- |---- |
| visible |  |  | 是否显示载入动画，支持任意类型 |
| size | String | '50px' | 动画效果大小 |
| height | String |  | 动画窗口高度 |
| center | Boolean | true | 是否在父元素居中位置显示 |

### 依赖

| 类型 | 描述 |
| ---- | ---- |
| quasar | QInnerLoading QSpinnerDots |


