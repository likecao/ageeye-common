# Head

## 组件

| 组件 | 描述 |
| ---- | ---- |
| AHead | 用户头像 |
| AHeads | 用户头像组 |

## AHead

用于显示单个用户的头像、用户名等信息

### 属性

| 属性 | 类型 | 默认值 | 描述 |
| ---- | ---- | ---- |---- |
| user* | Object | |用户对象 |
| size | Number | 32 | 头像大小 |
| text-size | Number | | 用户名大小，默认根据size自动计算 |
| username | Boolean | true | 是否显示用户名 |
| head | Boolean | true | 是否显示头像,size<16会隐藏 |
| slogan | Boolean | true | 是否显示简介,size<32会隐藏 |
| level | Boolean | false | 是否显示级别,前提必须显示头像 |
| popover | Boolean | true | 鼠标悬停时是否显示用户信息卡片 |

### 全局事件

| 事件 | 描述 |
| ---- | ---- |
| @openMessageDialog | 点击私信按钮后打开发送私信对话框 |

### 依赖

| 类型 | 描述 |
| ---- | ---- |
| quasar | QBtn QIcon QChip QPopover |
| vuex | store.state.auth |
| ageeye | ALoading |
| api | /api/user/follow/ /api/user/{id}/statistic/ |

### 示例

```
<template>
  <div>
    <a-head :user="user"/>
    <a-head :user="user" :size="24"/>
  </div>
</template>

<script>
export default {
  data(){
    return {
      user: {
        id: 1,
        username: '香又甜',
        head: 'https://...url.jpg',
        level: 8,
        slogan: '我是隔壁老王'
      }
    }
  }
}
</script>

```

## AHeads

用于显示一组用户头像、用户名等信息，比如显示地图的作者和协作者。

### 属性

| 属性 | 类型 | 默认值 | 描述 |
| ---- | ---- | ---- |---- |
| user* | Object | |用户对象 |
| editor | Array | | 协作者用户对象列表 |
| dense | Boolean | true | 是否启用密集模式，启用后将不显示用户名 |
| size | Number | 32 | 头像大小 |
| username | Boolean | true | 是否显示用户名 |
| head | Boolean | true | 是否显示头像,size<16过小时会隐藏 |
| slogan | Boolean | true | 是否显示简介,size<32过小时会隐藏 |

### 依赖

| 类型 | 描述 |
| ---- | ---- |
| ageeye | AHead |

