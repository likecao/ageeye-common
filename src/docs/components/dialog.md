# Dialog

## 组件

| 组件 | 描述 |
| ---- | ---- |
| ADialog | 弹出对话框 |
| AShareDialog | 弹出分享对话框 |

## ADialog

弹出对话框,支持v-model绑定任意值，控制窗口的隐藏

### 属性

| 属性 | 类型 | 默认值 | 描述 |
| ---- | ---- | ---- |---- |
| title | String  |  | 对话框标题 |
| icon |  String |  | 对话框图标 |

### 事件

| 事件 | 描述 |
| ---- | ---- |
| @hide | 隐藏对话框 |

### 插槽

| 名称 | 描述 |
| ---- | ---- |
| default | 显示对话框内容 |

### 依赖

| 类型 | 描述 |
| ---- | ---- |
| quasar | QBtn QIcon QModal |


### 示例

```
<template>
  <div>
    <a-dialog title="这是标题" icon="user" v-model="open">
      嘿嘿嘿
    </a-dialog>
  </div>
</template>

<script>
export default {
  data(){
    return {
      open: false
    }
  }
}
</script>

```

## AShareDialog

弹出分享对话框，必须配合ADialog使用

### 属性

| 属性 | 类型 | 默认值 | 描述 |
| ---- | ---- | ---- |---- |
| title* | String |  | 分享标题 |
| link | String |  | 分享链接，如果为空则使用当前页面url |

### 依赖

| 类型 | 描述 |
| ---- | ---- |
| quasar | QBtn QInput |
| github | @xkeshi/vue-qrcode |
