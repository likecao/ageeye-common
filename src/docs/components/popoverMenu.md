# PopoverMenu

## 组件

| 组件 | 描述 |
| ---- | ---- |
| APopoverMenu | 按钮菜单组件 |

## APopoverMenu
按钮下拉菜单组件

### 属性

| 属性 | 类型 | 默认值 | 描述 |
| ---- | ---- | ---- |---- |
| menu | Array |  | 菜单配置 |
| instance | Object |  | 数据对象 |

### 依赖

| 类型 | 描述 |
| ---- | ---- |
| quasar | QBtn QPopover QList QItem QIcon |

### 菜单选项

menu菜单配置是由多个菜单项配置构成的数组。菜单项支持属性如下：
| 属性 | 类型 |描述 | 传入参数 |
| ---- | ---- | ---- | ---- |
| name* | String | 菜单名称 | |
| icon | String | 图标名称 | |
| separator | Boolean | 是否在菜单前面启用分隔线 | |
| visible | Boolean Function | 菜单可见性 | instance |
| action | Function | 菜单动作 | instance |


### 示例

```
<template>
  <div>
    <a-popover-menu :menu="menu" :instance="instance"/>
  </div>
</template>

<script>
export default {
  data(){
    return {
      instance: {
        id: 5,
        title: '这是一篇文章',
        add_user: {
          username: '曹操'
        },
        summary: '...',
        is_public: false
      },
      menu: [
        {
          name: '发布',
          icon: 'delete',
          visible: (instance)=>{
            return !instance.is_public
          },
          action: (instance)=>{
            ...
          }
        }
      ]
    }
  }
}
</script>

```

