# Comment

## 组件

| 组件 | 描述 |
| ---- | ---- |
| ACommentBox | 单条评论 |
| ACommentBoxes | 评论组和评论发布框 |

## ACommentBox

单条评论

### 属性

| 属性 | 类型 | 默认值 | 描述 |
| ---- | ---- | ---- |---- |
| comment* | Object |  | 评论对象 |
| map | Object |  | 地图对象，显示添加在地图中的评论的缩略图 |
| index | Number |  | 这条评论在评论组的序号 |

### 事件

| 事件 | 描述 |
| ---- | ---- |
| @reply | 回复当前评论 |

### 全局事件

| 事件 | 描述 |
| ---- | ---- |
| @toggleReplyInput | 切换回复评论框，用于关闭其他评论组件已经打开的回复框 |

### 依赖

| 类型 | 描述 |
| ---- | ---- |
| quasar | QBtn QIcon QItem QItemMain |
| vuex | store.state.auth |
| ageeye | AMapViewer |
| github | sanitizeHtml |

### 示例

```
<template>
  <a-comment-box :comment="comment" :map="map" />
</template>

<script>
export default {
  data(){
    return {
      comment: {
        id: 3,
        user: {
          id: 1,
          username: '香又甜',
          ...
        },
        parent: {
          id: 2,
          ...
        }
        ...
      },
      map: {
        id: 337,
        file: 'https://...json'
        name: '中国古代军事地理',
        ...
      }
    }
  }
}
</script>

```

## ACommentBoxes

评论组和评论发布框

### 属性

| 属性 | 类型 | 默认值 | 描述 |
| ---- | ---- | ---- |---- |
| id* | Number |  | 被评论对象的ID，如地图ID、文章ID |
| title* | String |  | 评论块标题，如评论、回复，会显示在多个地方 |
| type* | Object |  | 评论对象类型，如map、forum等 |
| titleHidden | Boolean |  | 是否隐藏顶部的大标题 |
| map | Object |  | 地图对象，显示添加在地图中的评论的缩略图 |


### 全局事件

| 事件 | 描述 |
| ---- | ---- |
| @toggleReplyInput | 切换回复评论框，用于关闭其他评论组件已经打开的回复框 |

### 依赖

| 类型 | 描述 |
| ---- | ---- |
| quasar | QBtn QList |
| vuex | store.state.auth |
| ageeye | ACommentBox AEditor |
| api | /api/{type}/{id}/comment/ |
