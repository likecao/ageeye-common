import * as componentsDocs from './components/index'
import * as utilsDocs from './utils/index'
import Components from './components.md'
import Mixins from './mixins.md'
import Utils from './utils.md'
import Vue from 'vue'

const docs = {
  ...componentsDocs,
  ...utilsDocs,
  Components,
  Mixins,
  Utils
}

console.log(docs)

const install = function () {
  Object.keys(docs).forEach((key) => {
    Vue.component('Docs' + key, docs[key])
  })
}

export default {
  install
}
