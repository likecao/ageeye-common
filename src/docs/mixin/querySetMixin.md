# Button

给地图、评论、讨论、文章等对象进行点赞、收藏、分享的按钮

## 组件

| 组件 | 描述 |
| ---- | ---- |
| AUp | 点赞按钮 |
| ACollect | 收藏按钮 |
| AShare | 分享按钮 |

## AUp
点赞按钮

### 属性

| 属性 | 类型 | 默认值 | 描述 |
| ---- | ---- | ---- |---- |
| type* | String |  | 对象类型，如map、forum、comment等 |
| id* | String |  | 对象id |
| up | Number | 0 | 点赞数 |
| upd | Boolean | false | 是否点赞 |

### 依赖
| 类型 | 描述 |
| ---- | ---- |
| quasar | QBtn QIcon |
| vuex | store.state.auth |
| api | /api/{type}/{id}/up/ |

## ACollect
点赞按钮

### 属性

| 属性 | 类型 | 默认值 | 描述 |
| ---- | ---- | ---- |---- |
| type* | String |  | 对象类型，如map、forum、comment等 |
| id* | String |  | 对象id |
| collected | Boolean | false | 是否收藏 |

### 依赖
| 类型 | 描述 |
| ---- | ---- |
| quasar | QBtn QIcon |
| vuex | store.state.auth |
| api | /api/{type}/{id}/collect/ |

## AShare
点赞按钮

### 属性

| 属性 | 类型 | 默认值 | 描述 |
| ---- | ---- | ---- |---- |
| title* | String |  | 分享标题 |
| link | String |  | 分享链接，如果为空则使用当前页面url |

### 依赖
| 类型 | 描述 |
| ---- | ---- |
| quasar | QBtn QIcon |
| ageye | ADialog AShareDialog |
