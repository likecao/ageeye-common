# components

一个基于quasar framework组件开发的vue组件库

## 源码

位于`ageeye-quasar-common/src/components/`目录

## 组件列表

| 类别 | 描述 | 组件 |
| ---- | ---- | ---- |
| [Comment](/components/Comment/) | 用户评论 | ACommentBox ACommentBoxes |
| [Head](/components/Head/) | 用户头像 | AHead AHeads |
| [Map](/components/Map/) | 地图浏览相关 | AMapImageBox AMapImageBoxes AMapViewer |
| [Button](/components/Button/) | 动作按钮 | ACollect AUp AShare |
| [Dialog](/components/Dialog/) | 弹出对话框 | ADialog, AShareDialog |
| [Editor](/components/Editor/) | 富文本编辑器 | AEditor |
| [Loading](/components/Loading/) | 正在加载提示 | ALoading |
| [None](/components/None/) | 无内容提示 | None |
| [Notify](/components/Notify/) | 用户通知列表 | ANotify, ANotifications |
| [Oauth](/components/Oauth/) | 第三方登录按钮 | AOauth |
| [TextInput](/components/TextInput/) | 纯文本编辑器 | ATextInput |
| [Upload](/components/Upload/) | 文件上传 | AUpload |
| [Search](/components/Search/) | 搜索框 | ASearch, ASearchUser |

## 使用方法

### 局部注册

```
<template>
  <div>
    <a-head :user="user"/>
    <a-up :type="map" :id="337" :up="1534"/>
  <div/>
</template>

<script>
import {Head, Up} from 'ageeye-quasar-common'

export default {
  components: {Head, Up}
  data(){
    return {
      user: {
        id: 1,
        username: '香又甜',
        head: 'https://...url.jpg',
        level: 8,
        slogan: '我是隔壁老王'
      }
    }
  }
}
</script>
```

### 全局注册

在`src/components/index.js`中导入
```
/**
 * src/components/index.js
 */
import Vue from 'vue'

import {
  Head,
  Heads,
  MapImageBoxes,
  Dialog,
  None,
  Collect,
  Share,
  ShareDialog,
  Upload,
  TextInput,
  CommentBox,
  CommentBoxes,
  Oauth,
  Up

} from 'ageeye-quasar-common'

const components = {
  Head,
  Heads,
  MapImageBoxes,
  Dialog,
  None,
  Collect,
  Share,
  ShareDialog,
  Upload,
  TextInput,
  CommentBox,
  CommentBoxes,
  Oauth,
  Up
}

const install = function () {
  Object.keys(components).forEach((key) => {
    Vue.component(components[key].name, components[key])
  })
}

export default {
  install
}
```

在`src/plugins/boot.js`中注册
```
/**
 * src/plugins/boot.js
 */
import Common from '../components/index'

export default ({ app, router, store, Vue }) => {
  Vue.use(Common)

  ...
}
```

