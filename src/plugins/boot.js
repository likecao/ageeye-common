/* eslint-disable no-new */

import Common from '../components/index'
import Docs from '../docs/index'

export default ({ app, router, store, Vue }) => {
  Vue.use(Common)
  Vue.use(Docs)

  if (process.env.NODE_ENV === 'development') {
    app = new Vue(app)
    window.app = app
  } else {
    new Vue(app)
  }
}
