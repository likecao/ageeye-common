/**
 * Created by CL on 2017/3/19.
 */
import Vue from 'vue'

import Head from './head/head.vue'
import Heads from './head/heads.vue'
import CommentBox from './comment/commentBox.vue'
import CommentBoxes from './comment/commentBoxes.vue'
import MapImageBox from './map/mapImageBox.vue'
import MapImageBoxes from './map/mapImageBoxes.vue'
import TextInput from './textInput.vue'
import Dialog from './dialog/dialog.vue'
import ShareDialog from './dialog/shareDialog.vue'
import None from './none.vue'
import Oauth from './oauth.vue'
import Collect from './button/collect.vue'
import Share from './button/share.vue'
import Upload from './upload.vue'
import Up from './button/up.vue'
import Notifications from './notify/notifications.vue'
import Notify from './notify/notify.vue'
import Editor from './editor.vue'
import Pagination from './pagination.vue'
import PopoverMenu from './popoverMenu.vue'
import Search from './search/search.vue'
import SearchUser from './search/searchUser.vue'
import Loading from './loading.vue'

const components = {
  Head,
  Heads,
  CommentBox,
  CommentBoxes,
  MapImageBox,
  MapImageBoxes,
  TextInput,
  Dialog,
  None,
  Oauth,
  Collect,
  Share,
  ShareDialog,
  Upload,
  Up,
  Notifications,
  Notify,
  Editor,
  Pagination,
  PopoverMenu,
  Search,
  SearchUser,
  Loading
}

const install = function () {
  Object.keys(components).forEach((key) => {
    Vue.component(components[key].name, components[key])
  })
}

export {
  Head,
  Heads,
  CommentBox,
  CommentBoxes,
  MapImageBox,
  MapImageBoxes,
  TextInput,
  Dialog,
  None,
  Oauth,
  Collect,
  Share,
  ShareDialog,
  Upload,
  Up,
  Notifications,
  Notify,
  Editor,
  Pagination,
  PopoverMenu,
  Search,
  SearchUser,
  Loading
}

export default {
  install
}
