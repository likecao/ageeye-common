/**
 *
 * @param {string} string
 * @param {Object} params
 * @returns {string}
 */

export function format (string, params) {
  for (const key in params) {
    const re = new RegExp('{' + key + '}', 'g')
    string = string.replace(re, params[key])
  }
  return string
}

/**
 *
 * @param {string} content
 * @returns {string}
 */
export function handleBreak(content) {
  const textArray = content.split('\n')

  let result = ''

  textArray.forEach(string => {
    result += `<p>${string}</p>`
  })

  return result
}

/**
 *
 * @param {string} string
 */
export function slice(string, length) {
  if (string.length > length){
    return string.slice(0, length) + '...'
  } else {
    return string
  }
}

/**
 *
 * @param {string} html
 * @param {Array} tags
 * @returns {*}
 */
export function sanitizeHtml (html, tags) {
  const sanitizeHtml = require('sanitize-html')
  return sanitizeHtml(html, {
    allowedTags: tags
  })
}


