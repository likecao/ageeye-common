import { Cookies } from 'quasar'

export const TOKEN = 'ageeyeToken'
export const USER = 'ageeyeUser'
export const SESSION = 'ageeyeSession'
export const EXPIRES = 'ageeyeExpires'

export function saveToken (token, user, session) {
  const expires = new Date()
  const options = {
    path: '/'
  }

  if (session) {
    expires.setDate(expires.getDate() + 30)
    options.expires = expires

    Cookies.set(EXPIRES, expires.toISOString(), options)
  }


  Cookies.set(TOKEN, token, options)
  Cookies.set(USER, user, options)
  Cookies.set(SESSION, session, options)
}

export function getToken() {
  return Cookies.get(TOKEN)
}

export function clearToken () {
  Cookies.remove(TOKEN, {path: '/'})
  Cookies.remove(USER, {path: '/'})
  Cookies.remove(SESSION, {path: '/'})
  Cookies.remove(EXPIRES, {path: '/'})
}
