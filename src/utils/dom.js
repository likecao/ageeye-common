/**
 *
 * @param {Element} element
 */
export function scrollToElement (element) {
  const offsetTop = element.offsetTop
  document.documentElement.scrollTop = offsetTop
}

export function dataURLtoFile(dataUrl, filename) {
  const arr = dataUrl.split(','), mime = arr[0].match(/:(.*?);/)[1]
  let bstr = atob(arr[1]), n = bstr.length, u8arr = new Uint8Array(n)

  while (n--) {
    u8arr[n] = bstr.charCodeAt(n)
  }

  return new File([u8arr], filename, {type: mime, lastModified: Date.now()})
}


