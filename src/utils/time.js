
const chunks = [
  [1000 * 60 * 60 * 24 * 365, '年'],
  [1000 * 60 * 60 * 24 * 30, '月'],
  [1000 * 60 * 60 * 24 * 7, '周'],
  [1000 * 60 * 60 * 24, '天'],
  [1000 * 60 * 60, '小时'],
  [1000 * 60, '分钟']
]

/**
 *
 * @param {string} dateString
 * @returns {string}
 */
export function timesince (dateString) {
  const delta = Date.now() - new Date(dateString)

  for (let i = 0; i < chunks.length; i++) {
    const item = chunks[i]
    const [seconds, name] = item
    const count = Math.floor(delta / seconds)

    if (count > 0) {
      return count + name + '前'
    }
  }

  return '刚才'
}
