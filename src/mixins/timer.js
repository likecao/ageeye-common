export const timerMixin = {
  data(){
    return {
      timer: false,
      timeout: 60,
      counter: 0
    }
  },
  computed: {
    countdown(){
      return this.timeout - this.counter
    }
  },
  methods: {
    startTimer(){
      if (this.timer){
        return null
      }

      this.timer = true
      this.$interval = setInterval(() => {
        if (this.counter >= this.timeout) {
          window.clearInterval(this.$interval)
          this.counter = 0
          this.timer = false
        }
        this.counter += 1
      }, 1000)
    }
  }
}
