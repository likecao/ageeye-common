const routes = [
  {
    path: '/',
    component: () => import('layouts/MyLayout.vue'),
    children: [
      { path: '', component: () => import('pages/index.vue') },
      { path: 'mixins', component: () => import('pages/docs.vue'), props: {docs: 'Mixins'} },
      { path: 'components', component: () => import('pages/docs.vue'), props: {docs: 'Components'} },
      { path: 'utils', component: () => import('pages/docs.vue'), props: {docs: 'Utils'} },
      { path: 'components/:docs', component: () => import('pages/docs.vue'), props: true },
      { path: 'utils/:docs', component: () => import('pages/docs.vue'), props: true }
    ]
  }
]

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue')
  })
}

export default routes
